#! /usr/bin/python

import sys
import datetime
import sqlite3
import re
import json
from collections import defaultdict

tracked = defaultdict( lambda : defaultdict( lambda : 0 ) )
defaults = defaultdict( lambda : ( None, 0 ) )

def track( kind ) :
    def tracker( column, value ) :
        if value :
            global tracked, defaults
            count = tracked[kind][value] + 1
            if ( count > defaults[kind][1] ) :
                defaults[kind] = ( value, count )
            tracked[kind][value] = count
        return value
    return tracker

class SkipRow (Exception) :
    pass

def first( n ) :
    def inner( c, v ) :
        return v[:n]
    return inner

def not_null( column, value ) :
    if value and len( value ) > 0 :
        return value
    raise SkipRow

def non_zero( column, value ) :
    if not value :
        raise SkipRow
    return value

def value_map( column, value ) :
    try :
        return enums[column][value]
    except KeyError, e :
        print "Could not map value", column, ':', value, '   ', e

def ts_to_iso( column, value ) :
    return datetime.datetime.fromtimestamp( float( value ) / 1000000.0 ).isoformat() + "+00:00"

def cc_to_watchers( column, value ) :
    return [ x.strip() for x in value.split(',') ] if value and len( value ) > 0 else None

def trim( column, value ) :
    value = value.strip()
    if len( value ) == 0 :
        return None
    return value

def default_nt( column, value ) :
    if value and len( value ) > 0 :
        return value
    return "n/t"

def markup( column, value ) :
    s = value.strip()
    if len( s ) > 0 :
        # naive. for now
        # - replace ticket links
        s = re.sub( "[[]ticket:([0-9]*) [^]]*]", r"Issue #\1", s )
    return s

pk = 1
def auto_pk( column, value ) :
    global pk
    pk += 1
    return pk

enums = {
    "kind" : { "defect":"bug", "enhancement":"enhancement", "wish":"proposal", "task":"task" }
    , "priority" : { "trivial":"trivial", "minor":"minor", "major":"major", "critical":"critical", "blocker":"blocker" }
    , "status" : {"new":"new", "accepted":"open", "closed":"resolved", "worksforme":"invalid", "duplicate":"duplicate", "wontfix":"wontfix" } # no "on hold"
}

mapping = {
"issues" : {
    "source" : "ticket"
    , "type" : "dict"
    , "mapping" : {
        "id" : ( "id", non_zero )
        , "kind" : ( "type", ( value_map, not_null ) ) 
        , "created_on" : ( "time", ( ts_to_iso, not_null ) )
        , "content_updated_on" : ( "changetime", ( ts_to_iso, not_null ) )
        , "edited_on" : ( "changetime", ts_to_iso )
        , "updated_on" : ( "changetime", ts_to_iso )
        , "component" : ( "component", trim, track( "component" ) )
        , "priority" : ( "priority", ( value_map, not_null ) )
        , "assignee" : ( "owner", ( trim, track( "assignee" ) ) )
        , "reporter" : ( "reporter", trim )
        , "watchers" : ( "cc", cc_to_watchers ) # BB ignores watchers
        , "version" : ( "version", trim )
        , "milestone" : ( "milestone", trim )
        , "status" : ( "status", ( value_map, not_null ) )
        , "title" : ( "summary", ( markup, first(255), trim, not_null ) )
        , "content" : ( "description", ( markup, trim, default_nt ) )
        # resolution, keywords, severity
        }
    }
, "comments" : {
    "source" : "ticket_change"
    , "filter" : [ ( "field", "=", "comment" ) ]
    , "mapping" : {
        "content" : ( "newvalue", ( markup, not_null ) )
        , "created_on" : ( "time", ts_to_iso )
        , "id" : ( "time", auto_pk )
        , "issue" : "ticket"
        , "updated_on" : ( "time", ts_to_iso )
        , "user" : "author"
        }
    }
, "attachments" : {
    "source" : "attachment"
    , "filter" : [ ( "type", "=", "ticket" ) ]
    , "mapping" : {
        "filename" : "filename"
        , "issue" : "id"
        #, "path" : 
        , "user" : "author"
        }
    }
, "components" : {
    "source" : "component"
    , "mapping" : {
        "name" : ( "name", track( "component" ) )
        }
    }
, "milestones" : {
    "source" : "milestone"
    , "mapping" : {
        "name" : "name"
        }
    }
, "versions" : {
    "source" : "version"
    , "mapping" : {
        "name" : "name"
        }
    }
, "log" : {
    "source" : "ticket_change"
    , "filter" : [ ( "field", "!=", "comment" ) ]
    , "mapping" : {
        "changed_from" : ( "oldvalue", first(255) )
        , "changed_to" : ( "newvalue", first(255) )
        , "created_on" : ( "time", ts_to_iso )
        , "field" : ( "field", ( first(32), not_null ) )
        , "issue" : "ticket"
        , "user" : "author"
        # "comment"
        }
    }
}

out = {}

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

if __name__ == "__main__" :
    if len( sys.argv ) >= 3 :
        con = None
        try:
            conn = sqlite3.connect( sys.argv[1] )
            conn.row_factory = dict_factory
            cur = conn.cursor()
            for dest, data in mapping.iteritems() :
                table = data['source']
                try :
                    where = []
                    for term in data['filter'] :
                        where.append( "( %s %s '%s' )" % term )
                    where = "WHERE " + " and ".join( where )
                except KeyError :
                    where = ""
                sql = "SELECT * from %s %s" % ( table, where )
                print sql
                records = []
                cur.execute( sql )
                for row in cur :
                    try :
                        try :
                            mapper = data['mapping']
                        except KeyError :
                            record = row[data['values']]
                        else :
                            record = {}
                            for dest_attr, src_attr in mapper.iteritems() :
                                if hasattr( src_attr, "strip" ) : # string
                                    record[dest_attr] = row[src_attr]
                                else :
                                    v = row[src_attr[0]]
                                    fn = src_attr[1]
                                    if hasattr( fn, "__call__" ) :
                                        v = src_attr[1]( dest_attr, v )
                                    else :
                                        for subfn in fn :
                                            v = subfn( dest_attr, v )
                                    record[dest_attr] = v
                    except SkipRow :
                        print "skipping row", row
                    else :
                        records.append( record )
                if len( records ) > 0 :
                    out[dest] = records
        except sqlite3.Error, e:
            print "SQLite3 Error %s:" % e.args[0]
            sys.exit(1)
        finally:
            if conn:
                conn.close()
        out["meta"] = {
            "default_assignee": defaults["assignee"][0]
            , "default_component": defaults["component"][0]
            , "default_kind": "bug"
            , "default_milestone": None
            , "default_version": None
        }
        with open( sys.argv[2], 'wb' ) as outfile:
            json.dump( out, outfile )
    else :
        print "** usage:", sys.argv[0], "<input.db>", "<output.json>"
