trac2bb
=======

A simple Python tool for exporting issues from a Trac project into the format needed by BitBucket issue tracker imports.

**Usage**

```
./trac2bb.py <path-to-trac-input.db> <path-to-bb-output.json>
```

**Notes**

* The utility does not require the Trac python libraries. It runs on a vanilla Python2.6+ installation.
* you will still need to zip the json file for import.
* It does not handle attachments.  This would be added if the utility generated zip files.
* Very little mapping of issue data occurs. Specifically no user mapping, etc.

**References**

* [BitBucket import format docs](https://confluence.atlassian.com/pages/viewpage.action?pageId=330796872)
* [Trac DB Schema](http://trac.edgewall.org/wiki/TracDev/DatabaseSchema)
